import {
    COUNTRY_STAT_LIST_REQUESTING,
    COUNTRY_STAT_LIST_SUCCESS,
    COUNTRY_STAT_LIST_ERROR,
} from './covid19Constants';
import Covid19Services from '../../services/covid19Services';
import { call, takeLatest, put } from 'redux-saga/effects';
function* getCountryStatList(action) {
    try {
        const countriesListResponse = yield call(Covid19Services.getCountriesList);
        const countriesList = countriesListResponse && countriesListResponse.Countries ? countriesListResponse.Countries : [];
        
        if(action.sortByTotalInfectedPeople === 'asc'){
            countriesList.sort((a, b) => (a.TotalConfirmed > b.TotalConfirmed) ? 1 : -1);
        } else {
            countriesList.sort((a, b) => (a.TotalConfirmed < b.TotalConfirmed) ? 1 : -1);
        }

        const countriesListSliced = countriesList.length > action.listSize ? countriesList.slice(0,action.listSize) : countriesList;
        if (countriesListSliced.length > 0) {
            yield put({
                type: COUNTRY_STAT_LIST_SUCCESS,
                records: countriesListSliced,
            });
        } else {
            yield put({
                type:  COUNTRY_STAT_LIST_ERROR,
            });
        }
    } catch (error) {
        yield put({ type:  COUNTRY_STAT_LIST_ERROR });
    }
}

function* getCountryStatListWatcher() {
    yield takeLatest( COUNTRY_STAT_LIST_REQUESTING, getCountryStatList);
}

const covid19Sagas = [getCountryStatListWatcher()];

export default covid19Sagas;
