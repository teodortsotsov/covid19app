import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCountryStatList } from './Covid19Actions';
import { countriesListSelector } from './Covid19Selectors';
import FadeIn from '../utils/FadeIn';
import './CountryDetails.scss';

class CountryDetails extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount(){
        if(!this.props.countries.records || this.props.countries.records.length === 0){
            this.props.getCountryStatList(10000, 'desc');
        }
    }
    render(){

        const { countryCode } = this.props.match.params;
        const selectedCountry = this.props.countries.records.find(element => element.CountryCode === countryCode);
        return (
            selectedCountry ? (<FadeIn><div className='details info'>
                <div className='item'>Country Code: {selectedCountry.CountryCode}</div>
                <div className='item'>Country Name: {selectedCountry.Country}</div>
                <div className='item'>New confirmed: {selectedCountry.NewConfirmed}</div>
                <div className='item deaths'>New deaths: {selectedCountry.NewDeaths}</div>
                <div className='item recovered'>New recovered: {selectedCountry.NewRecovered}</div>
                <div className='item'>Date reported: {new Date(selectedCountry.Date).toLocaleDateString("en-US")}</div>
                <input type='button' className='history' value='Show History' /> 
            </div></FadeIn>) : null

        )
    }
}

const mapStateToProps = (state) => ({
    countries: countriesListSelector(state),
});

const mapDispatchToProps = (dispatch) => {
    return {
        getCountryStatList: (listSize, sortByTotalInfectedPeople) => dispatch(getCountryStatList(listSize, sortByTotalInfectedPeople)),
    };
};

CountryDetails.propTypes = {
    selectedCountry: PropTypes.objectOf(
        PropTypes.shape({
            Country: PropTypes.string,
            CountryCode: PropTypes.string,
        })
    ),
    getCountryStatList: PropTypes.func,
    countries: PropTypes.arrayOf(PropTypes.objectOf({})),
};

export default connect(mapStateToProps,mapDispatchToProps)(CountryDetails);
