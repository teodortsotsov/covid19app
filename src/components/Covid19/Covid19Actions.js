import { COUNTRY_STAT_LIST_REQUESTING } from './Covid19Constants';

export const getCountryStatList = (listSize, sortByTotalInfectedPeople) => ({ type: COUNTRY_STAT_LIST_REQUESTING, listSize, sortByTotalInfectedPeople });
