import { combineReducers } from 'redux';
import { countriesListReducer } from '../components/Covid19/Covid19Reducers';

const rootReducer = combineReducers({ countriesListReducer });

export default rootReducer;
