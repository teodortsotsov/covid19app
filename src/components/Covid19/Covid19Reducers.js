import {
    COUNTRY_STAT_LIST_REQUESTING,
    COUNTRY_STAT_LIST_SUCCESS,
    COUNTRY_STAT_LIST_ERROR,
} from './Covid19Constants';

export const countriesListReducer = (state = { records: [] }, action) => {
    switch (action.type) {
        case COUNTRY_STAT_LIST_REQUESTING:
            return {
                ...state,
            };
        case COUNTRY_STAT_LIST_SUCCESS:
            return {
                ...state,
                records: action.records,
            };
        case COUNTRY_STAT_LIST_ERROR:
            return {
                ...state,
                error: action.error,
            };
        default:
            return state;
    }
};
