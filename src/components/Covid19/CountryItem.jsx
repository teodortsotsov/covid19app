import React from 'react';
import PropTypes from 'prop-types';

const CountryItem = (props) => {
    const { country, key, selected, selectCountry } = props;
    const name = country.Country;
    const { TotalConfirmed, TotalRecovered } = country;
    return (
        <li
            key={key}
            className={selected ? 'active' : ''}
            onClick={() => {
                selectCountry(country);
            }}
        >
            <div className="country head">{name}</div>
            <div className="country-info">
                <span>Total number of infected people: {TotalConfirmed}</span>
                <span>Total number of recovered  people: {TotalRecovered}</span>
            </div>
        </li>
    );
}

CountryItem.propTypes = {
    country: PropTypes.objectOf(
        PropTypes.shape({
            Country: PropTypes.string,
            CountryCode: PropTypes.string,
        })
    ),
    selectCountry: PropTypes.func,
    selected: PropTypes.bool,
};

export default CountryItem;
