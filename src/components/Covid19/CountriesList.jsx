import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getCountryStatList } from './Covid19Actions';
import CountryItem from './CountryItem';
import CountryDetails from './CountryDetails';
import { countriesListSelector } from './Covid19Selectors';
import './Covid19.scss';

class CountriesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedCountry: { CountryCode: -1, Country: '' },
            listSize: 10,
            sortByTotalInfectedPeople: 'desc',
        };
    }
    componentDidMount() {
        const { listSize, sortByTotalInfectedPeople } = this.state;
        this.props.getCountryStatList(listSize, sortByTotalInfectedPeople);
    }
    componentDidUpdate(prevProps, prevState) {
        const { listSize, sortByTotalInfectedPeople } = this.state;
        if(prevState.sortByTotalInfectedPeople !== sortByTotalInfectedPeople || prevState.listSize !== listSize){
            this.props.getCountryStatList(listSize, sortByTotalInfectedPeople);
        }
    }
    selectCountry = (country) => {
        this.setState({ selectedCountry: country });
    };
    handleSortChange = () => {
        const { sortByTotalInfectedPeople } = this.state;
        const newSort = sortByTotalInfectedPeople === 'desc' ? 'asc' : 'desc';
        this.setState({sortByTotalInfectedPeople: newSort})
    }
    handleListSizeChange = () => {
        const {listSize} = this.state;
        this.setState({listSize: listSize + 10})
    }
    render() {
        const { selectedCountry, sortByTotalInfectedPeople } = this.state;
        const matchObject = { params:{countryCode: selectedCountry.CountryCode}}
        return (
            <div id="covid19">
                <div id="countries-list">
                    <div className="head list-head">Sort by number of infected people
                    <i className={`arrow ${sortByTotalInfectedPeople}`} onClick={this.handleSortChange}></i>
                    </div>
                    <ul>
                        {this.props.countries.records.map((country) => {
                            return (
                                <CountryItem
                                    key={country.CountryCode}
                                    selected={
                                        country.CountryCode === selectedCountry.CountryCode
                                    }
                                    country={country}
                                    selectCountry={this.selectCountry}
                                />
                                
                            );
                        })}
                    </ul>
                    <div>
                    <input type='button' id='show-next' onClick={this.handleListSizeChange} value='Show next 10 countries' /> 
                    </div>
                </div>

                {selectedCountry.CountryCode !== -1 ? (
                    <CountryDetails selectedCountry match={matchObject}/>
                ) : (
                    <Fragment />
                )}
            </div>
        );
    }
}
const mapStateToProps = (state) => ({
    countries: countriesListSelector(state),
});

const mapDispatchToProps = (dispatch) => {
    return {
        getCountryStatList: (listSize, sortByTotalInfectedPeople) => dispatch(getCountryStatList(listSize, sortByTotalInfectedPeople)),
    };
};

CountriesList.propTypes = {
    getCountryStatList: PropTypes.func,
    countries: PropTypes.arrayOf(PropTypes.objectOf({})),
};

export default connect(mapStateToProps, mapDispatchToProps)(CountriesList);
