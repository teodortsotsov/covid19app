import axios from 'axios';
import { host } from '../../server/serverConstants';
const url = host;
const Covid19ervices = {
    getCountriesList: function () {
        let countriesList = [];
        return axios
            .get(url + '/summary')
            .then(function (response) {
                if (response.data) {
                    countriesList = response.data;
                }
                return countriesList;
            })
            .catch(function (error) {
                console.log('Error getting infected people list' + error);
                return countriesList;
            });
    },
};

export default Covid19ervices;
