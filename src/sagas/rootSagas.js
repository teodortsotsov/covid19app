import { all } from 'redux-saga/effects';
import covid19Sagas from '../components/Covid19/Covid19Sagas';

export default function* rootSagas() {
    yield all([...covid19Sagas]);
}
