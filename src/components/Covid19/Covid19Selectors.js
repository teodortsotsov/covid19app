import { createSelector } from 'reselect';

const getCountriesList = (state) => state.countriesListReducer;

export const countriesListSelector = createSelector(
    getCountriesList,
    (coutriesList) => coutriesList
);
