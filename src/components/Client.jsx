import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from '../store/configureStore';
import CountriesList from './Covid19/CountriesList';
import CountryDetails from './Covid19/CountryDetails';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './Client.scss';

class Client extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className="client">
                    <BrowserRouter>
                        <Switch>
                            <Route path="/details/:countryCode" render={props => <CountryDetails {...props}/>} />
                            <Route render={props => <CountriesList  {...props} />} />
                        </Switch>
                    </BrowserRouter>
                </div>
            </Provider>
        );
    }
}
export default Client;
